package fr.arolla.romannumerals.domain;


interface RomanNumerals {

    String toRoman(PositiveInteger numericValue);
}
